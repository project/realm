Realm

Realm provides an entity-based alternative to Taxonomies for grouping content.
It is based on the premise that Taxonomies are a content property, and
shouldn't be used for non-Content groupings.


Credits
-------
Realm was built based on the Model Entity project
(https://drupal.org/project/model).